package ru.t1.dsinetsky.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.component.Bootstrap;

public class Application {

    public static void main(@NotNull final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}

