package ru.t1.dsinetsky.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public interface ICommand {

    void execute() throws GeneralException;

    @NotNull
    String getName();

    @NotNull
    String getDescription();

    @Nullable
    String getArgument();

    @Nullable
    Role[] getRoles();

}
