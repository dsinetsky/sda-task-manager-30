package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.exception.GeneralException;

public interface ITestCreateService {

    void createTest() throws GeneralException;

}
