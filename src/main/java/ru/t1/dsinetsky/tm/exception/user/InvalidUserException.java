package ru.t1.dsinetsky.tm.exception.user;

public final class InvalidUserException extends GeneralUserException {

    public InvalidUserException() {
        super("Invalid user!");
    }

}
