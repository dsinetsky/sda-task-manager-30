package ru.t1.dsinetsky.tm.exception.user;

public final class UserNotLoggedException extends GeneralUserException {

    public UserNotLoggedException() {
        super("You are not logged in!");
    }

}
