package ru.t1.dsinetsky.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public final class EntityIdIsEmptyException extends GeneralFieldException {

    public EntityIdIsEmptyException(@NotNull final String modelName) {
        super(modelName + " id cannot be empty!");
    }

}
