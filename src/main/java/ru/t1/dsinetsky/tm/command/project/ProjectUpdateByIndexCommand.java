package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_UPD_PROJECT_BY_INDEX;

    @NotNull
    public static final String DESCRIPTION = "Updates name and description of project (if any) found by index";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter new name:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final Project project = getProjectService().updateByIndex(userId, index, name, description);
        showProject(project);
        System.out.println("Project successfully updated!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
