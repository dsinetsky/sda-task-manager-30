package ru.t1.dsinetsky.tm.command.user.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.builder.repository.UserBuilder;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.User;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserCreateCommand extends AbstractAdminCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_CREATE_USER;

    @NotNull
    public static final String DESCRIPTION = "Creates new user";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login of user:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("Enter user role");
        @Nullable final String roleValue = TerminalUtil.nextLine();
        @NotNull final Role role = Role.toRole(roleValue);
        System.out.println("Enter email:");
        @Nullable final String email = TerminalUtil.nextLine();
        System.out.println("Enter first name:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        @NotNull final User user = UserBuilder.create()
                .login(login)
                .password(password)
                .role(role)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .middleName(middleName)
                .toUser();
        getUserService().add(user);
        System.out.println("User successfully created!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
