package ru.t1.dsinetsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String CONFIG_FILE = "/logger.properties";

    @NotNull
    private static final String MESSAGE = "message";

    @NotNull
    private static final String MESSAGE_FILE = "./message.log";

    @NotNull
    private static final String COMMAND = "command";

    @NotNull
    private static final String COMMAND_FILE = "./command.log";

    @NotNull
    private static final String ERROR = "error";

    @NotNull
    private static final String ERROR_FILE = "./error.log";

    @NotNull
    private static final Logger MESSAGE_LOGGER = getMessageLogger();

    @NotNull
    private static final Logger ERROR_LOGGER = getErrorLogger();

    @NotNull
    private static final Logger COMMAND_LOGGER = getCommandLogger();

    @NotNull
    private final LogManager manager = LogManager.getLogManager();

    @NotNull
    private final Logger root = Logger.getLogger("");

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        init();
        initLogger(MESSAGE_LOGGER, MESSAGE_FILE, true);
        initLogger(ERROR_LOGGER, ERROR_FILE, true);
        initLogger(COMMAND_LOGGER, COMMAND_FILE, false);
    }

    @NotNull
    public static Logger getMessageLogger() {
        return Logger.getLogger(MESSAGE);
    }

    @NotNull
    public static Logger getErrorLogger() {
        return Logger.getLogger(ERROR);
    }

    @NotNull
    public static Logger getCommandLogger() {
        return Logger.getLogger(COMMAND);
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {

            @Override
            @NotNull
            public String format(@NotNull final LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void initLogger(@NotNull final Logger logger, @NotNull final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGE_LOGGER.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGE_LOGGER.fine(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        ERROR_LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        COMMAND_LOGGER.info(message);
    }

}
